import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import 'typeface-roboto';
import Typography from '@material-ui/core/Typography';
import scrollToComponent from 'react-scroll-to-component';
import Chip from '@material-ui/core/Chip';
import AceEditor from 'react-ace'
import axios from 'axios'
import 'brace/mode/java';
import 'brace/theme/dracula';
import './App.css';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import SimpleExpansionPanel from './SimpleExpansionPanel'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#44475a',
    },
    secondary: {
      main: '#aaaaaa',
    },
    error: {
      main: '#ffffff',
    }
  },
  typography: {
    useNextVariants: true,
  },
});

const buttonStyle = {
  width: "100%",
  height: "50px",
};

const amarginv = {
  marginTop: "45px",
  marginBottom: "35px"
}

var listItems = [];
var correctAns = 0;
var totalProb = 0;
var compileErrorMessage = ""
var isCompileError = false;

class App extends Component {


  constructor(props) {
    super(props)
    this.state = {
      value: "",
      nyan: false,
      labelWidth: 0,
      age: "TP2",
      result: [],
      showResult: false,
    }
    this.onChange = this.onChange.bind(this)
    this.kirim = this.kirim.bind(this)
  }

  onChange(newValue) {
    // this.state.value = newValue;
    this.setState({value: newValue})
  }


  async kirim() {
    if (this.state.value === null) {
      return
    }
    this.setState({
      nyan: true,
      showResult: false,
    })
    scrollToComponent(this.refs.nani)
    var url = 'http://aren-api.herokuapp.com/grader/'
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
    url = 'http://localhost:8080/grader/'
    // url = 'http://aren-api.herokuapp.com/grader/'
  }
    await axios.post(url, {
      code: this.state.value,
      name: this.state.age,
    }).then(res => {
      isCompileError = res.data["isCompileError"];
      compileErrorMessage = res.data["compileErrorMessage"];
      const results = res.data["caseResults"];
      totalProb = results.length
      correctAns = 0
      listItems = results.map((result, index) => {
        let props = {
          duration: result.duration / 1000000,
          answer: result.answer,
          expected: result.expected,
          problem: result.problem,
          timeout: result.timeout,
          pass: result.pass,
          number: index,
        }

        if (result.pass) {
          correctAns++
        }
        if (result.timeout){
          return (
            <div>
              <Paper className="paperuu">
                    <Typography variant="h5" component="h3">
                        #{index+1}
                         <Chip label="Time Limit Exceed" style={{ backgroundColor: '#ff5555', color: '#f8f8f2' }} className="chip" />}
                    </Typography>
              </Paper>
            </div>
          )
        }

        return (
          <SimpleExpansionPanel {...props} key={index+1}/>
        )
      }
      );
    });

    this.setState({
      nyan: false,
      showResult: true,
    })
  }


  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };


  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Typography variant="h2" gutterBottom style={amarginv}>
            AREN 2.1
      </Typography>
          <div className="some">
            <Paper elevation={5} className="editor">
              <AceEditor
                showPrintMargin={false}
                mode="java"
                theme="dracula"
                name="editor"
                fontSize={17}
                onChange={this.onChange}
                editorProps={{ $blockScrolling: true }}
                width="795px"
                height="600px"
                value={this.state.value}
                highlightActiveLine={false}

              />
            </Paper>
          </div>

          {/* <Grid container spacing={24} style={aHeight}> */}
          <Grid container spacing={24}>
            <Grid item xs={8}>
              <FormControl style={buttonStyle}>
                <Select
                  value={this.state.age}
                  onChange={this.handleChange}
                  displayEmpty
                  name="age"
                  style={buttonStyle}
                >
                  <MenuItem value={"TP1"}>TP1</MenuItem>
                  <MenuItem value={"TP2"}>TP2</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={4}>
              <Button variant="contained" color="primary" onClick={this.kirim} style={buttonStyle}>
                Grade!
            </Button>
            </Grid>
          </Grid>
          <div id="nani" ref="nani" className="aempty">
            {this.state.nyan && <Child />}
            {this.state.showResult && <Result />}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}


const Result = () => {
  if (isCompileError) {
    return (
      <div>
        <div className="result">
          <Typography variant="h4" gutterBottom className="result">
            Compile Error:
              </Typography>
        </div>
        <div>
          <Typography>
            {compileErrorMessage}
          </Typography>
        </div>
      </div>
    )
  } else {
    return (
      <div>
        <div className="result">
          <Typography variant="h4" gutterBottom className="result">
            Success: {correctAns}/{totalProb}
          </Typography>
        </div>
        <div>
          {listItems}
        </div>
      </div>
    )
  }
}

const Child = () => (
  <div>
    <div>
      <img src="https://i.imgur.com/Spi2hgG.gif" className="nyan" alt="nyancat"/>
    </div>
    <div>
      <Typography variant="h4" gutterBottom>
        grading...
      </Typography>
    </div>
  </div>
)
export default App;
